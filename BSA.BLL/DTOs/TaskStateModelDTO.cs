﻿namespace BSA.BLL.DTOs
{
    [System.Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}
