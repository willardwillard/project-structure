﻿using BSA.DataAccess.Entities;

namespace BSA.BLL.DTOs.LinqDTOs
{
    public class UserTasksCountDTO
    {
        public Project Project { get; set; }
        public int TaskCount { get; set; }
    }
}
