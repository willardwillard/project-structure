﻿using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
        }
    }
}