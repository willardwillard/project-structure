﻿using BSA.BLL.DTOs;
using BSA.BLL.Services.Interfaces;
using BSA.DataAccess.UnityOfWork;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<TeamWithOlderUsersDTO> GetListOfOlderUsers()
        {
            return _unitOfWork.Users.GetAll().Where(x => x.Birthday.Year > 2007)
                .OrderByDescending(x => x.RegisteredAt)
                .GroupBy(x => x.TeamId)
                .Join(_unitOfWork.Teams.GetAll(), x => x.Key, y => y.Id, (x, y) => new TeamWithOlderUsersDTO()
                {
                    teamId = y.Id,
                    teamName = y.Name,
                    users = x.ToList()
                });
        }
    }
}
