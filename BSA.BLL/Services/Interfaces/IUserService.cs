﻿using System.Collections.Generic;
using BSA.BLL.DTOs;

namespace BSA.BLL.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<SortedUsersDTO> GetSortedUsers();
        UserTasksDataDTO GetUserTasksData(int userId);
    }
}