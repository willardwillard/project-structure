﻿using System.Collections.Generic;
using BSA.BLL.DTOs.LinqDTOs;
using BSA.DataAccess.Entities;

namespace BSA.BLL.Services.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<UserTasksCountDTO> GetNumberOfUserTasks(int userId);
        IEnumerable<Task> GetListOfUserTasks(int userId);
        Dictionary<int, string> GetFinishedTasks(int userId);
    }
}