﻿using BSA.DataAccess.UnityOfWork;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetAll()
        {
            return Ok(_unitOfWork.Users.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_unitOfWork.Users.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserDTO item)
        {
            _unitOfWork.Users.Create(_mapper.Map<UserDTO, User>(item));
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] UserDTO item)
        {
            _unitOfWork.Users.Update(_mapper.Map<UserDTO, User>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.Users.Delete(id);
            return NoContent();
        }
    }
}
