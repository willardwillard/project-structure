﻿using BSA.BLL.DTOs;
using BSA.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly ITeamService _teamService;
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;

        public DataController(IProjectService projectService,
            ITeamService teamService,
            ITaskService taskService,
            IUserService userService)
        {
            _projectService = projectService;
            _teamService = teamService;
            _taskService = taskService;
            _userService = userService;
        }

        [Route("projectTasks/{id}")]
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjectTasksInfo(int id)
        {
            return Ok(_projectService.GetProjectTasksInfo(id));
        }

        [Route("olderUsers")]
        [HttpGet]
        public ActionResult<IEnumerable<TeamWithOlderUsersDTO>> GetListOfOlderUsers()
        {
            return Ok(_teamService.GetListOfOlderUsers());
        }

        [Route("numberOfUserTasks/{id}")]
        [HttpGet]
        public ActionResult<Dictionary<ProjectDTO, int>> GetNumberOfUserTasks(int id)
        {
            return Ok(_taskService.GetNumberOfUserTasks(id));
        }

        [Route("userTasks/{id}")]
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetListOfUserTasks(int id)
        {
            return Ok(_taskService.GetListOfUserTasks(id));
        }

        [Route("finishedTasks/{id}")]
        [HttpGet]
        public ActionResult<Dictionary<int, string>> GetFinishedTasks(int id)
        {
            return Ok(_taskService.GetFinishedTasks(id));
        }

        [Route("userTasksData/{id}")]
        [HttpGet]
        public ActionResult<UserTasksDataDTO> GetUserTasksData(int id)
        {
            return Ok(_userService.GetUserTasksData(id));
        }

        [Route("sortedUsers")]
        [HttpGet]
        public ActionResult<IEnumerable<SortedUsersDTO>> GetSortedUsers()
        {
            return Ok(_userService.GetSortedUsers());
        }
    }
}