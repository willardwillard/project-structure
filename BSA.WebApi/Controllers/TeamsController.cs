﻿using BSA.DataAccess.UnityOfWork;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetAll()
        {
            return Ok(_unitOfWork.Teams.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_unitOfWork.Teams.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] TeamDTO item)
        {
            _unitOfWork.Teams.Create(_mapper.Map<TeamDTO, Team>(item));
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] TeamDTO item)
        {
            _unitOfWork.Teams.Update(_mapper.Map<TeamDTO, Team>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
            return NoContent();
        }
    }
}
