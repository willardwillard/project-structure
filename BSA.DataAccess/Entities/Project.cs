﻿using Newtonsoft.Json;

namespace BSA.DataAccess.Entities
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public System.DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public System.DateTime Deadline { get; set; }
        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}" +
                   $"\nName: {Name}" +
                   $"\nDescription: {Description}" +
                   $"\nCreated: {CreatedAt.ToShortDateString()}" +
                   $"\nDeadline: {Deadline.ToShortDateString()}" +
                   $"\nAuthor_id: {AuthorId}" +
                   $"\nTeam_id: {TeamId}";
        }
    }
}
