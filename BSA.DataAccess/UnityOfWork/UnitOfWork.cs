﻿using BSA.DataAccess.Entities;
using BSA.DataAccess.Repository;

namespace BSA.DataAccess.UnityOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IRepository<Project> _projects = new ProjectRepository();
        private readonly IRepository<User> _users = new UserRepository();
        private readonly IRepository<Task> _tasks = new TaskRepository();
        private readonly IRepository<Team> _teams = new TeamRepository();

        public IRepository<Project> Projects
        {
            get => _projects;
        }

        public IRepository<User> Users
        {
            get => _users;
        }

        public IRepository<Team> Teams
        {
            get => _teams;
        }

        public IRepository<Task> Tasks
        {
            get => _tasks;
        }

        public void Save()
        {
            //Save changes to db
        }
    }
}
