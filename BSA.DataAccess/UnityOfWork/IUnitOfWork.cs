﻿using BSA.DataAccess.Entities;
using BSA.DataAccess.Repository;

namespace BSA.DataAccess.UnityOfWork
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<User> Users { get; }
        IRepository<Team> Teams { get; }
        IRepository<Task> Tasks { get; }

        void Save();
    }
}
