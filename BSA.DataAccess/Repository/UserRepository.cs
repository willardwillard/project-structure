﻿using System;
using BSA.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.DataAccess.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly List<User> _users = new List<User>();

        public void Create(User item)
        {
            if (_users.Any())
            {
                var x = _users.Last();
                item.Id = x.Id + 1;
                _users.Add(item);
            }
            else
            {
                item.Id = 1;
                _users.Add(item);
            }
        }

        public User Get(int id)
        {
            return _users.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Delete(int id)
        {
            _users.RemoveAt(_users.FindIndex(x => x.Id == id));
        }

        public void Update(User item)
        {
            var user = _users.FirstOrDefault(x => x.Id == item.Id);
            if (user != null)
            {
                var index = _users.FindIndex(x => x.Id == item.Id);
                _users[index] = item;
            }
        }
    }
}