﻿using System;
using BSA.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.DataAccess.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly List<Task> _tasks = new List<Task>();

        public void Create(Task item)
        {
            if (_tasks.Any())
            {
                var x = _tasks.Last();
                item.Id = x.Id + 1;
                _tasks.Add(item);
            }
            else
            {
                item.Id = 1;
                _tasks.Add(item);
            }
        }

        public Task Get(int id)
        {
            return _tasks.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _tasks;
        }

        public void Delete(int id)
        {
            _tasks.RemoveAt(_tasks.FindIndex(x => x.Id == id));
        }

        public void Update(Task item)
        {
            var task = _tasks.FirstOrDefault(x => x.Id == item.Id);
            if (task != null)
            {
                var index = _tasks.FindIndex(x => x.Id == item.Id);
                _tasks[index] = item;
            }
        }
    }
}
